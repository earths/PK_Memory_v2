package com.pkparis.pk_memory_v2.Receiver;


import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.pkparis.pk_memory_v2.MainActivity;


public class USBAutoRun extends BroadcastReceiver {

    private static final String ACTION_USB_PERMISSION = "com.pkparis.pk_memory_v2.USB_PERMISSION";
    UsbManager mUsbManager = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        
        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        PendingIntent mPermissionIntent;
        mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        mUsbManager.requestPermission(device, mPermissionIntent);

        if (ACTION_USB_PERMISSION.equals(action)) {
            device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                if (device != null) {
                    Intent i = new Intent(context, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            }
        }
    }
}
