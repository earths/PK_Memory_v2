package com.pkparis.pk_memory_v2.Listener;

import android.view.View;


public interface RecyclerClick_Listener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}

