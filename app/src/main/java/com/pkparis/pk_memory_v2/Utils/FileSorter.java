package com.pkparis.pk_memory_v2.Utils;


import com.pkparis.pk_memory_v2.Adapter.BaseFile;

import java.util.Comparator;

public class FileSorter implements Comparator<BaseFile> {

    private int dirsOnTop = 0;
    private int asc = 1;
    private int sort = 0;

    public FileSorter(int dir, int sort, int asc) {
        this.dirsOnTop = dir;
        this.asc = asc;
        this.sort = sort;
    }

    private boolean isDirectory(BaseFile path) {
        return path.isDirectory();
    }

    @Override
    public int compare(BaseFile file1, BaseFile file2) {
        if (dirsOnTop == 0) {
            if (isDirectory(file1) && !isDirectory(file2)) {
                return -1;
            } else if (isDirectory(file2) && !isDirectory(file1)) {
                return 1;
            }
        } else if (dirsOnTop == 1) {
            if (isDirectory(file1) && !isDirectory(file2)) {
                return 1;
            } else if (isDirectory(file2) && !isDirectory(file1)) {
                return -1;
            }
        }
        if (sort == 0) {
            // sort by name
            return asc * file1.getName().compareToIgnoreCase(file2.getName());
        } else if (sort == 1) {
            // sort by last modified
            return asc * Long.valueOf(file1.getDate()).compareTo(file2.getDate());
        } else if (sort == 2) {
            // sort by size
            dirsOnTop = 1;
            if (!file1.isDirectory() && !file2.isDirectory()) {
                return asc * Long.valueOf(file1.getSize()).compareTo(file2.getSize());
            } else {
                return file1.getName().compareToIgnoreCase(file2.getName());
            }
        } else if(sort ==3) {
            // sort by type
            dirsOnTop = 1;
            if(!file1.isDirectory() && !file2.isDirectory()) {
                final String ext_a = getExtension(file1.getName());
                final String ext_b = getExtension(file2.getName());
                final int res = asc*ext_a.compareTo(ext_b);
                if (res == 0) {
                    return asc * file1.getName().compareToIgnoreCase(file2.getName());
                }
                return res;
            } else {
                return  file1.getName().compareToIgnoreCase(file2.getName());
            }
        }
        return 0;
    }

    private static String getExtension(String a) {
        return a.substring(a.lastIndexOf(".") + 1).toLowerCase();
    }

}
