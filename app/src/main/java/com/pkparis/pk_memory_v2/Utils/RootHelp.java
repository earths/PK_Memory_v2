package com.pkparis.pk_memory_v2.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import com.pkparis.pk_memory_v2.Adapter.BaseFile;
import com.pkparis.pk_memory_v2.MainActivity;

import java.io.File;
import java.util.ArrayList;

public class RootHelp {


    private static final String PREFIX_OTG = "otg:/";


    public static ArrayList<BaseFile> getDocumentFilesList(String path, Context context) {
        SharedPreferences manager = PreferenceManager.getDefaultSharedPreferences(context);
        String rootUriString = manager.getString(MainActivity.KEY_PREF_OTG, null);

        DocumentFile rootUri = DocumentFile.fromTreeUri(context, Uri.parse(rootUriString));
        ArrayList<BaseFile> files = new ArrayList<>();

        String[] parts = path.split("/");
        for (String part : parts) {

            // first omit 'otg:/' before iterating through DocumentFile
            if (path.equals(PREFIX_OTG + "/")) break;
            if (part.equals("otg:") || part.equals("")) continue;
            Log.d(context.getClass().getSimpleName(), "Currently at: " + part);
            // iterating through the required path to find the end point
            rootUri = rootUri.findFile(part);
        }

        Log.d(context.getClass().getSimpleName(), "Found URI for: " + rootUri.getName());
        // we have the end point DocumentFile, list the files inside it and return
        for (DocumentFile file : rootUri.listFiles()) {
            try {
                if (file.exists()) {
                    long size = 0;
                    if (!file.isDirectory()) size = file.length();
                    Log.d(context.getClass().getSimpleName(), "Found file: " + file.getName());
                    BaseFile baseFile = new BaseFile(path + "/" + file.getName(),
                            parseDocumentFilePermission(file), file.lastModified(), size, file.isDirectory());
                    baseFile.setName(file.getName());
                    files.add(baseFile);
                }
            } catch (Exception ignored) {
            }
        }
        return files;
    }



    public static ArrayList<BaseFile> getFilesList(String path) {
        File f = new File(path);
        ArrayList<BaseFile> files = new ArrayList<>();
        try {
            if (f.exists() && f.isDirectory()) {
                for (File x : f.listFiles()) {
                    long size = 0;
                    if (!x.isDirectory()) size = x.length();
                    BaseFile baseFile = new BaseFile(x.getPath(), parseFilePermission(x),
                            x.lastModified(), size, x.isDirectory());
                    baseFile.setName(x.getName());
                    if (!x.isHidden()){
                        files.add(baseFile);
                    }
                }
            }
        } catch (Exception ignored) {
        }
        return files;
    }

    private static String parseDocumentFilePermission(DocumentFile file) {
        String per = "";
        if (file.canRead()) {
            per = per + "r";
        }
        if (file.canWrite()) {
            per = per + "w";
        }
        if (file.canWrite()) {
            per = per + "x";
        }
        return per;
    }

    private static String parseFilePermission(File f) {
        String per = "";
        if (f.canRead()) {
            per = per + "r";
        }
        if (f.canWrite()) {
            per = per + "w";
        }
        if (f.canExecute()) {
            per = per + "x";
        }
        return per;
    }



}
