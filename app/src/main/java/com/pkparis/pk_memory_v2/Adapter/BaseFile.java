package com.pkparis.pk_memory_v2.Adapter;

import android.os.Parcel;
import android.os.Parcelable;

import com.pkparis.pk_memory_v2.Utils.RootHelp;

public class BaseFile extends RootHelp implements Parcelable {


    private long date, size;
    private boolean isDirectory;
    private String permission;
    private String name;
    private String path = "";
    private boolean animate;


    public BaseFile(String path, String permission, long date, long size, boolean isDirectory) {
        this.date = date;
        this.size = size;
        this.isDirectory = isDirectory;
        this.path = path;
        this.permission = permission;

    }

    private BaseFile(Parcel in) {
        date = in.readLong();
        size = in.readLong();
        isDirectory = in.readByte() != 0;
        permission = in.readString();
        name = in.readString();
        path = in.readString();
    }

    public static final Creator<BaseFile> CREATOR = new Creator<BaseFile>() {
        @Override
        public BaseFile createFromParcel(Parcel in) {
            return new BaseFile(in);
        }

        @Override
        public BaseFile[] newArray(int size) {
            return new BaseFile[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public String getPath() {
        return path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeString(permission);
        dest.writeString(name);
        dest.writeLong(date);
        dest.writeLong(size);
        dest.writeByte((byte) (isDirectory ? 1 : 0));

    }

    public long getSize() {
        return size;
    }

    void setAnimate(boolean animating) {
        animate = animating;
    }

    boolean getAnimating() {
        return animate;
    }

}