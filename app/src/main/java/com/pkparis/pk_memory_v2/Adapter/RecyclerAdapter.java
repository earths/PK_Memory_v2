package com.pkparis.pk_memory_v2.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.pkparis.pk_memory_v2.MainActivity;
import com.pkparis.pk_memory_v2.R;
import com.pkparis.pk_memory_v2.Ui.IconHolder;
import com.pkparis.pk_memory_v2.Ui.Icons;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.String.valueOf;


public class RecyclerAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private ArrayList<BaseFile> arrayList;
    private Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private BaseFile mFile;
    private ItemViewHolder mHolder;
    private IconHolder mIconHolder;
    public boolean stoppedAnimation = false;
    private int isList;
    private int offset = 0;
    private MainActivity m;

    private int videoColor;
    private int audioColor;
    private int pdfColor;
    private int codeColor;
    private int textColor;
    private int archiveColor;
    private int genericColor;
    private int apkColor;
    private int dirColor;


    private static final int VIDEO = 0, AUDIO = 1, PDF = 2, CODE = 3, TEXT = 4, ARCHIVE = 5,
            GENERIC = 6, APK = 7, PICTURE = 8;

    public RecyclerAdapter(MainActivity m , Context context, ArrayList<BaseFile> arrayList, int isList) {
        this.m = m;
        this.context = context;
        this.arrayList = arrayList;
        this.isList = isList;
        mSelectedItemsIds = new SparseBooleanArray();

        videoColor = getColor(context, R.color.video_item);
        audioColor = getColor(context, R.color.audio_item);
        pdfColor = getColor(context, R.color.pdf_item);
        codeColor = getColor(context, R.color.code_item);
        textColor = getColor(context, R.color.text_item);
        archiveColor = getColor(context, R.color.archive_item);
        genericColor = getColor(context, R.color.generic_item);
        dirColor = getColor(context, R.color.dir_item);
        apkColor = getColor(context, R.color.apk_item);
        mIconHolder = new IconHolder(context,true,false);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup;
        switch (isList){
            case 1:
                mainGroup = (ViewGroup) mInflater.inflate(R.layout.file_content_grid_2, parent, false);
                break;
            case 2:
                mainGroup = (ViewGroup) mInflater.inflate(R.layout.file_content_grid, parent, false);
                break;
            default:
                mainGroup = (ViewGroup) mInflater.inflate(R.layout.file_content, parent, false);
                break;
        }
        return new ItemViewHolder(mainGroup);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        mFile = arrayList.get(position);
        mHolder = holder;
        mHolder.about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, view);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.rename:

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.detail_menu);
                popupMenu.show();
            }
        });
        //
        if (!this.stoppedAnimation && !mFile.getAnimating()) {
            animate(mHolder.itemView);
            mFile.setAnimate(true);
        }
        //
        try{
            setHolderIcon(mFile);
            setHolderLabel();
            mHolder.itemView.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4 : Color.TRANSPARENT);
            mHolder.about.setColorFilter(Color.parseColor("#ff666666"));
            setHolderDesc();
            setHolderCount();
        }catch (Exception ignored){
        }
    }

    private void animate(View viewToAnimate) {
        viewToAnimate.clearAnimation();
        Animation localAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_in_top);
        localAnimation.setStartOffset(this.offset);
        viewToAnimate.startAnimation(localAnimation);
        this.offset += 30;
    }

    private void setHolderCount(){
        if (mFile.isDirectory()) {
            String count = valueOf(new File(mFile.getPath()).listFiles().length);
            if (count.compareTo("0") == 0) {
                mHolder.count.setText(R.string.empty);
            } else if (count.compareTo("1") == 0) {
                mHolder.count.setText(count + " " + context.getResources().getString(R.string.file));
            } else {
                mHolder.count.setText(count + " " + context.getResources().getString(R.string.files));
            }
        }else {
            String size = Formatter.formatShortFileSize(context, mFile.getSize());
            mHolder.count.setText(size);
        }

    }

    private void setHolderDesc(){
        mHolder.desc.setText(DateFormat.getDateInstance().format(new Date(mFile.getDate())));
    }

    private void setHolderLabel(){
        mHolder.label.setText(mFile.getName());
    }

    private void setHolderIcon(BaseFile mFile){

        GradientDrawable gradientDrawable = (GradientDrawable) mHolder.icon.getBackground();
        if (mFile.isDirectory()){
            mHolder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_grid_folder_new));
        }else {
            mHolder.icon.setImageDrawable(Icons.loadMimeIcon(mFile.getName(), false, context.getResources() ));
        }
        if (mFile.isDirectory()) {
            gradientDrawable.setColor(dirColor);
        } else {
            switch (Icons.getTypeOfFile(mFile.getPath())) {
                case VIDEO:
                    gradientDrawable.setColor(videoColor);
                    mIconHolder.cancelLoad(mHolder.icon);
                    mIconHolder.loadDrawable(mHolder.icon,mFile.getPath(),null);
                    break;
                case AUDIO:
                    gradientDrawable.setColor(audioColor);
                    mIconHolder.cancelLoad(mHolder.icon);
                    mIconHolder.loadDrawable(mHolder.icon,mFile.getPath(),null);
                    break;
                case PDF:
                    gradientDrawable.setColor(pdfColor);
                    break;
                case CODE:
                    gradientDrawable.setColor(codeColor);
                    break;
                case TEXT:
                    gradientDrawable.setColor(textColor);
                    break;
                case ARCHIVE:
                    gradientDrawable.setColor(archiveColor);
                    break;
                case GENERIC:
                    gradientDrawable.setColor(genericColor);
                    break;
                case APK:
                    gradientDrawable.setColor(apkColor);
                    mIconHolder.cancelLoad(mHolder.icon);
                    mHolder.icon.getBackground().setVisible(false,false);
                    mIconHolder.loadDrawable(mHolder.icon,mFile.getPath(),null);
                    break;
                case PICTURE:
                    try {
                        mIconHolder.cancelLoad(mHolder.icon);
                        mIconHolder.loadDrawable(mHolder.icon, mFile.getPath(), null);
                        if (mFile.getSize() == 0) {
                            gradientDrawable.setColor(genericColor);
                        }else {
                            mHolder.icon.setBackground(null);
                            mHolder.icon.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                            mHolder.icon.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                        }
                    }catch (Exception ignored){

                    }
                    break;
                default:
                    gradientDrawable.setColor(genericColor);
                    break;
            }
        }

    }

    public static int getColor(Context c, @ColorRes int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return c.getColor(color);
        } else {
            return c.getResources().getColor(color);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public File getSelectedFile(){
        for (int i = 0; i <= getItemCount(); i++) {
            if (getSelectedIds().get(i)) {
                return getItemFile(i);
            }
        }
        return null ;
    }


    public void toggleSelection(int position) {
        m.stopAnimation();
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    private void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, true);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }


    public File getItemFile(int position){
        return new File(arrayList.get(position).getPath());
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
