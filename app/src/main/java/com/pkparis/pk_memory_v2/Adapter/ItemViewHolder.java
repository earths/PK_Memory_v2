package com.pkparis.pk_memory_v2.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkparis.pk_memory_v2.R;

class ItemViewHolder extends RecyclerView.ViewHolder {

    TextView label;
    TextView desc;
    TextView count;
    ImageButton about;
    public ImageView icon;

    ItemViewHolder(View view) {
        super(view);
        label = (TextView) view.findViewById(R.id.label);
        desc = (TextView) view.findViewById(R.id.desc);
        count = (TextView) view.findViewById(R.id.count);
        icon = (ImageView) view.findViewById(R.id.icon);
        about = (ImageButton) view.findViewById(R.id.properties);
    }
}