package com.pkparis.pk_memory_v2.FileUtils;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.provider.DocumentFile;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pkparis.pk_memory_v2.MainActivity;
import com.pkparis.pk_memory_v2.R;
import com.pkparis.pk_memory_v2.Utils.MimeTypes;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import static com.pkparis.pk_memory_v2.FileUtils.CreateDocument.getUriFromFile;

public class MakeFile {

    public static void createNewFile(final MainActivity m){
        new MaterialDialog.Builder(m)
                .title("Create new file")
                .inputRangeRes(2, 20, R.color.selected_item)
                .positiveText("Save")
                .negativeText("Cancel")
                .input(null, null, new MaterialDialog.InputCallback() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        File to = new File(m.CURRENT_PATH, String.valueOf(input));
                        try {
                            if (MakeFile.makeFile(to, m.getBaseContext())){
                                Toast.makeText(m.getBaseContext(),R.string.mkfile_successful,Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(m.getBaseContext(),R.string.mkfile_error,Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(m.getBaseContext(),R.string.mkfile_error,Toast.LENGTH_LONG).show();
                        }
                        m.refreshDevice();
                    }
                }).show();
    }

    public static boolean makeFile(final File file, Context context) throws IOException {
        if(file==null)
            return false;
        if (file.exists()) {
            // nothing to create.
            return !file.isDirectory();
        }

        // Try the normal way
        try {
            if (file.createNewFile()) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Try with Storage Access Framework.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && CreateDocument.isOnExtSdCard(file, context)) {
            DocumentFile document = CreateDocument.getDocumentFile(file.getParentFile(), true, context);
            // getDocumentFile implicitly creates the directory.
            try {
                assert document != null;
                return document.createFile(MimeTypes.getMimeType(file), file.getName()) != null;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            try {
                return mkFileOPS(context, file);
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    private static boolean mkFileOPS(final Context context, final File file) {
        final OutputStream outputStream = getOutputStream(context, file.getPath());
        if (outputStream == null) {
            return false;
        }
        try {
            outputStream.close();
            return true;
        } catch (final IOException ignored) {
        }
        return false;
    }

    private static OutputStream getOutputStream(Context context, String str) {
        OutputStream outputStream = null;
        Uri fileUri = getUriFromFile(str, context);
        if (fileUri != null) {
            try {
                outputStream = context.getContentResolver().openOutputStream(fileUri);
            } catch (Throwable ignored) {
            }
        }
        return outputStream;
    }
}
