package com.pkparis.pk_memory_v2.FileUtils;


import android.content.Context;
import android.os.Build;
import android.support.v4.provider.DocumentFile;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pkparis.pk_memory_v2.MainActivity;
import com.pkparis.pk_memory_v2.R;

import java.io.File;

public class MakeDirectory {

    public static void createNewDir(final MainActivity m){
        new MaterialDialog.Builder(m)
                .title(R.string.mkdir_create)
                .inputRangeRes(2, 20, R.color.selected_item)
                .positiveText(R.string.save)
                .negativeText(R.string.cancel)
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        File to = new File(m.CURRENT_PATH, String.valueOf(input));
                        try {
                            if (MakeDirectory.makeDir(to,m.getBaseContext())){
                                Toast.makeText(m.getBaseContext(),R.string.mkdir_successful,Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(m.getBaseContext(),R.string.mkdir_error,Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(m.getBaseContext(),R.string.mkdir_error,Toast.LENGTH_LONG).show();
                        }
                        m.refreshDevice();
                    }
                }).show();
    }

    public static boolean makeDir(final File file, Context context) {
        if(file==null)
            return false;
        if (file.exists()) {
            // nothing to create.
            return file.isDirectory();
        }

        // Try the normal way
        if (file.mkdirs()) {
            return true;
        }
        if (file.mkdir()){
            return true;
        }

        // Try with Storage Access Framework.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && CreateDocument.isOnExtSdCard(file, context)) {
            DocumentFile document = CreateDocument.getDocumentFile(file, true, context);
            // getDocumentFile implicitly creates the directory.
            assert document != null;
            return document.exists();
        }

        return false;
    }


}
