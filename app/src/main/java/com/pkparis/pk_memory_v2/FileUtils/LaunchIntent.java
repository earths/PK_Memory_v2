package com.pkparis.pk_memory_v2.FileUtils;


import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.pkparis.pk_memory_v2.BuildConfig;
import com.pkparis.pk_memory_v2.MainActivity;
import com.pkparis.pk_memory_v2.R;
import com.pkparis.pk_memory_v2.Ui.Icons;
import com.pkparis.pk_memory_v2.Utils.MimeTypes;

import java.io.File;

public class LaunchIntent {

    public static void launchIntent(String filePath, MainActivity m){
        File file = new File(filePath);
        try {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri fileUri = FileProvider.getUriForFile(m.getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                String type = MimeTypes.getMimeType(file);
                intent.setDataAndType(fileUri, type);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                m.startActivity(intent);
            } catch (Exception e) {
                Uri uri = Uri.fromFile(new File(filePath));
                final Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                String type = MimeTypes.getMimeType(file);
                intent.setDataAndType(uri, type);
                m.startActivity(intent);
            }
        }catch (Exception e){
            openUnknown(file, m.getBaseContext(),true);
        }
    }

    private static void openUnknown(File f, Context c, boolean forceChooser) {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);

        String type = MimeTypes.getMimeType(f);
        if(type!=null && type.trim().length()!=0 && !type.equals("*/*")) {

            Uri uri=fileToContentUri(c, f);
            if(uri==null) uri=Uri.fromFile(f);
            intent.setDataAndType(uri, type);

            Intent startIntent;
            if (forceChooser) {
                startIntent=Intent.createChooser(intent, c.getResources().getString(R.string.open_with));
            } else {
                startIntent=intent;
            }
            try {
                c.startActivity(startIntent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private static final String EXTERNAL_VOLUME = "external";
    private static final String INTERNAL_VOLUME = "internal";
    private static Uri fileToContentUri(Context context, File file) {
        // Normalize the path to ensure media search
        final String normalizedPath = normalizeMediaPath(file.getAbsolutePath());

        // Check in external and internal storages
        Uri uri = fileToContentUri(context, normalizedPath, EXTERNAL_VOLUME);
        if (uri != null) {
            return uri;
        }
        uri = fileToContentUri(context, normalizedPath, INTERNAL_VOLUME);
        if (uri != null) {
            return uri;
        }
        return null;
    }

    private static Uri fileToContentUri(Context context, String path, String volume) {
        String[] projection;
        final String where = MediaStore.MediaColumns.DATA + " = ?";
        Uri baseUri = MediaStore.Files.getContentUri(volume);
        boolean isMimeTypeImage, isMimeTypeVideo = false, isMimeTypeAudio = false;
        isMimeTypeImage = Icons.isPicture( path);
        if (!isMimeTypeImage) {
            isMimeTypeVideo = Icons.isVideo(path);
            if (!isMimeTypeVideo) {
                isMimeTypeAudio = Icons.isVideo(path);
            }
        }
        if (isMimeTypeImage || isMimeTypeVideo || isMimeTypeAudio) {
            projection = new String[]{BaseColumns._ID};
            if (isMimeTypeImage) {
                baseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if (isMimeTypeVideo) {
                baseUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else {
                baseUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
        } else {
            projection = new String[]{BaseColumns._ID, MediaStore.Files.FileColumns.MEDIA_TYPE};
        }
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(baseUri, projection, where, new String[]{path}, null);
        try {
            if (c != null && c.moveToNext()) {
                boolean isValid;
                if (isMimeTypeImage || isMimeTypeVideo || isMimeTypeAudio) {
                    isValid = true;
                } else {
                    int type = c.getInt(c.getColumnIndexOrThrow(
                            MediaStore.Files.FileColumns.MEDIA_TYPE));
                    isValid = type != 0;
                }

                if (isValid) {
                    // Do not force to use content uri for no media files
                    long id = c.getLong(c.getColumnIndexOrThrow(BaseColumns._ID));
                    return Uri.withAppendedPath(baseUri, String.valueOf(id));
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    private static final String EMULATED_STORAGE_SOURCE = System.getenv("EMULATED_STORAGE_SOURCE");
    private static final String EMULATED_STORAGE_TARGET = System.getenv("EMULATED_STORAGE_TARGET");
    private static final String EXTERNAL_STORAGE = System.getenv("EXTERNAL_STORAGE");
    private static String normalizeMediaPath(String path) {
        // Retrieve all the paths and check that we have this environment vars
        if (TextUtils.isEmpty(EMULATED_STORAGE_SOURCE) ||
                TextUtils.isEmpty(EMULATED_STORAGE_TARGET) ||
                TextUtils.isEmpty(EXTERNAL_STORAGE)) {
            return path;
        }

        // We need to convert EMULATED_STORAGE_SOURCE -> EMULATED_STORAGE_TARGET
        if (path.startsWith(EMULATED_STORAGE_SOURCE)) {
            path = path.replace(EMULATED_STORAGE_SOURCE, EMULATED_STORAGE_TARGET);
        }
        return path;
    }
}
