package com.pkparis.pk_memory_v2.FileUtils;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.provider.DocumentFile;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pkparis.pk_memory_v2.MainActivity;
import com.pkparis.pk_memory_v2.R;
import com.pkparis.pk_memory_v2.Utils.MimeTypes;

import java.io.File;

public class Rename {

    public Rename (final MainActivity m){
        File from = m.mAdapter.getSelectedFile();
        new MaterialDialog.Builder(m)
                .title("Rename")
                .inputRangeRes(2, 20, R.color.selected_item)
                .positiveText("Save")
                .negativeText("Cancel")
                .input(null, from.getName(), new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        File from = m.mAdapter.getSelectedFile();
                        File to = new File(m.CURRENT_PATH, String.valueOf(input));
                        if (MimeTypes.isFileNameValid(to.getName())) {
                            rename(from, to, String.valueOf(input),m);
                        }
                    }
                }).show();
    }

    private void rename(File from , File to ,String input, MainActivity m){
        Boolean renamed;
        try {
            renamed = renameFile(from, String.valueOf(input)) || renameFolder(from, to, m);
            if (renamed) {
                Toast.makeText(m.getBaseContext(), R.string.rename_successful, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(m.getBaseContext(), R.string.rename_error, Toast.LENGTH_LONG).show();
            }
        } catch (Exception ignored){
            Toast.makeText(m.getBaseContext(),R.string.rename_error,Toast.LENGTH_LONG).show();
        }
        m.refreshDevice();
        m.mActionMode.finish();
    }

    private static boolean renameFile(File f, String name) {
        String newPath = f.getParent() + "/" + name;
        return f.getParentFile().canWrite() && f.renameTo(new File(newPath));
    }

    private static boolean renameFolder(@NonNull final File source, @NonNull final File target, Context context){
        // First try the normal rename.
        if (renameFile(source, target.getName())) {
            return true;
        }
        if (target.exists()) {
            return false;
        }

        // Try the Storage Access Framework if it is just a rename within the same parent folder.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && source.getParent().equals(target.getParent()) && CreateDocument.isOnExtSdCard(source, context)) {
            DocumentFile document = CreateDocument.getDocumentFile(source, true, context);
            assert document != null;
            if (document.renameTo(target.getName())) {
                return true;
            }
        }
        if (!MakeDirectory.makeDir(target, context)) {
            return false;
        }

        File[] sourceFiles = source.listFiles();

        if (sourceFiles == null) {
            return true;
        }

        for (File sourceFile : sourceFiles) {
            String fileName = sourceFile.getName();
            File targetFile = new File(target, fileName);
            if (!Copy.copyFile(sourceFile, targetFile, context)) {
                // stop on first error
                return false;
            }
        }
        // Only after successfully copying all files, delete files on source folder.
        for (File sourceFile : sourceFiles) {
            if (!Delete.deleteFile(sourceFile, context)) {
                // stop on first error
                return false;
            }
        }
        return true;
    }
}