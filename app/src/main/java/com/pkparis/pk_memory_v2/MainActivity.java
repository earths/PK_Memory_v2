package com.pkparis.pk_memory_v2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.pkparis.pk_memory_v2.Adapter.BaseFile;
import com.pkparis.pk_memory_v2.Adapter.RecyclerAdapter;
import com.pkparis.pk_memory_v2.FileUtils.Delete;
import com.pkparis.pk_memory_v2.FileUtils.LaunchIntent;
import com.pkparis.pk_memory_v2.FileUtils.MakeDirectory;
import com.pkparis.pk_memory_v2.FileUtils.MakeFile;
import com.pkparis.pk_memory_v2.FileUtils.Rename;
import com.pkparis.pk_memory_v2.Listener.RecyclerClick_Listener;
import com.pkparis.pk_memory_v2.Listener.RecyclerTouchListener;
import com.pkparis.pk_memory_v2.Utils.FileSorter;
import com.pkparis.pk_memory_v2.Utils.RootHelp;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Environment.getExternalStorageDirectory;


public class MainActivity extends AppCompatActivity {

    //
    private static final String ACTION_USB_PERMISSION = "com.pkparis.pk_memory_v2.USB_PERMISSION";
    public static final String KEY_PREF_OTG = "uri_usb_otg";
    private static final String URI_PERMISSION = "URI";
    private static final int REQUEST_CODE_SAF = 223;
    //
    UsbManager mUsbManager = null;
    IntentFilter mFilterAttachedAndDetached = null;
    private ArrayList<BaseFile> mFileArray = new ArrayList<>();
    public String CURRENT_PATH = getExternalStorageDirectory().toString() ;
    private boolean backPressedToExitOnce = false;
    //
    SharedPreferences sharedPref;
    private FloatingActionMenu mFloatingActionMenu;
    private SystemBarTintManager tintManager;
    //
    private RecyclerView mRecyclerView;
    private RelativeLayout mEmptyView;
    //
    public RecyclerAdapter mAdapter;
    private List<Integer> mScrollPosition = new ArrayList<>();
    private LinearLayoutManager mLayoutManager ;
    //
    public ActionMode mActionMode;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public Toolbar toolbar;
    //
    private File mCopyFile = null ;
    private int mIsList = 0;
    private int mSortType = 0;
    //
    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private View mDrawerHeaderLayout;


    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    refreshDevice();
                }
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                PendingIntent mPermissionIntent;
                mPermissionIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                mUsbManager.requestPermission(device, mPermissionIntent);
            } else if (ACTION_USB_PERMISSION.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    if (device != null) {
                        refreshDevice();
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_sort));
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null ;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        //
        mFloatingActionMenu = (FloatingActionMenu) findViewById(R.id.fab);
        FloatingActionButton fabNewFile = (FloatingActionButton) findViewById(R.id.menu_new_file);
        fabNewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeFile.createNewFile(getMainActivity());
                mFloatingActionMenu.close(true);
            }
        });
        FloatingActionButton fabNewDir = (FloatingActionButton) findViewById(R.id.menu_new_folder);
        fabNewDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeDirectory.createNewDir(getMainActivity());
                mFloatingActionMenu.close(true);
            }
        });
        //
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        //
        mFilterAttachedAndDetached = new IntentFilter();
        mFilterAttachedAndDetached.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        mFilterAttachedAndDetached.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        mFilterAttachedAndDetached.addAction(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, mFilterAttachedAndDetached);
        //
        if (SDK_INT == 20 || SDK_INT == 19) {
            tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintColor(getResources().getColor(R.color.colorPrimaryDark));
        } else if (SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.pkOrange));
        }
        //
        if (ContextCompat.checkSelfPermission(this,
                "android.permission.READ_EXTERNAL_STORAGE")
                != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    "android.permission.READ_EXTERNAL_STORAGE")) {
                ActivityCompat.requestPermissions(this,
                        new String[]{"android.permission.READ_EXTERNAL_STORAGE"},
                        0);
            }
        }


        //
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mEmptyView = (RelativeLayout) findViewById(R.id.recycler_empty);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mAdapter != null ) {
                    stopAnimation();
                }
                return false;
            }
        });
        implementRecyclerViewClickListeners();
        //
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDevice();
                mLayoutManager.scrollToPosition(0);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        //
        mDrawerList = (ListView) findViewById(R.id.menu_drawer);
        mDrawerHeaderLayout = getLayoutInflater().inflate(R.layout.nav_header_main, null , false);
        View mDrawerHeaderParent = mDrawerHeaderLayout.findViewById(R.id.navigation_header_container2);
        mDrawerList.addHeaderView(mDrawerHeaderParent);
        mDrawerList.setAdapter(null);
        //
        refreshDevice();
    }

    public MainActivity getMainActivity(){
        return this;
    }

    private void implementRecyclerViewClickListeners() {
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                if (!new File(CURRENT_PATH).exists()){
                    CURRENT_PATH = getExternalStorageDirectory().toString();
                }
                if (mActionMode != null) {
                    onListItemSelect(position);
                }else {
                    String filePath = mFileArray.get(position).getPath();
                    if (mFileArray.get(position).isDirectory()) {
                        CURRENT_PATH = filePath;
                        if (mLayoutManager != null){
                            mScrollPosition.add(mLayoutManager.findFirstCompletelyVisibleItemPosition());
                        }
                        refreshDevice();
                        mLayoutManager.scrollToPosition(0);
                    } else try {
                        if (mLayoutManager != null){
                            mScrollPosition.add(mLayoutManager.findFirstCompletelyVisibleItemPosition());
                        }
                        LaunchIntent.launchIntent(filePath,getMainActivity());
                    } catch (Exception e) {
                        Toast.makeText(getBaseContext(),"Error while launching",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                onListItemSelect(position);
            }
        }));
    }

    private void onListItemSelect(int position) {
        mAdapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = mAdapter.getSelectedCount() > 0;//Check if any items are already selected or not

        if (hasCheckedItems && mActionMode == null) {
            mActionMode = startSupportActionMode(mActionModeCallback);
            assert getSupportActionBar() != null ;
        } else if (!hasCheckedItems && mActionMode != null) {
            mActionMode.finish();
        }
        if (mActionMode != null)
            mActionMode.setTitle(String.valueOf(mAdapter.getSelectedCount())
                    + " "
                    + getResources().getString(R.string.file_selected) );

    }

    public ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            mFloatingActionMenu.hideMenuButton(true);

            updateViews(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

            getMenuInflater().inflate(R.menu.action_menu,menu);

            mSwipeRefreshLayout.setEnabled(false);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.select_all :
                    if (mAdapter.getSelectedIds().size() == mAdapter.getItemCount()){
                        mActionMode.finish();
                        break;
                    }
                    for(int i = 0; i <= mAdapter.getItemCount()-1; i++) {
                        if (!mAdapter.getSelectedIds().get(i))
                            onListItemSelect(i);
                    }
                    break;
                case R.id.copy:
                    mCopyFile = mAdapter.getSelectedFile();
                    refreshDevice();
                    mode.finish();
                    supportInvalidateOptionsMenu();
                    break;
                case R.id.cut:
                    break;
                case R.id.rename:
                    new Rename(getMainActivity());
                    break;
                case R.id.delete:
                    try {
                        for (int i = 0; i <= mAdapter.getItemCount(); i++) {
                            if (mAdapter.getSelectedIds().get(i)) {
                                File file = mAdapter.getItemFile(i);
                                Delete.deleteFile(file, getBaseContext());
                            }
                        }
                        Toast.makeText(getBaseContext(),R.string.delete_successful,Toast.LENGTH_LONG).show();
                    } catch (Exception ignored){
                        Toast.makeText(getBaseContext(),R.string.delete_error,Toast.LENGTH_LONG).show();
                    }
                    refreshDevice();
                    mode.finish();
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mFloatingActionMenu.showMenuButton(true);
            mAdapter.removeSelection();
            updateViews(new ColorDrawable(getResources().getColor(R.color.pkOrange)));
            mActionMode = null ;
            mSwipeRefreshLayout.setEnabled(true);
            refreshDevice();
        }

    };

    public void copy(File to) throws IOException {
        //
        File from = mAdapter.getSelectedFile();
        try (InputStream in = new FileInputStream(from)) {
            try (OutputStream out = new FileOutputStream(to)) {
                byte[] buf = new byte[2048]; // FLAPPY_BIRD
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
            refreshDevice();

    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == REQUEST_CODE_SAF && responseCode == Activity.RESULT_OK) {
            Uri treeUri;
            // Get Uri from Storage Access Framework.
            treeUri = intent.getData();
            // Persist URI - this is required for verification of writability.
            if (treeUri != null) sharedPref.edit().putString(URI_PERMISSION, treeUri.toString()).apply();

            sharedPref.edit().putString(KEY_PREF_OTG, intent.getData().toString()).apply();

            // After confirmation, update stored value of folder.
            // Persist access permissions.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                assert treeUri != null;
                getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        }
        refreshDevice();
    }


    public void updateViews(ColorDrawable colorDrawable) {
        assert getSupportActionBar() != null;

        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(colorDrawable.getColor());
        } else if (SDK_INT == 20 || SDK_INT == 19) {
            tintManager.setStatusBarTintColor(colorDrawable.getColor());
        }
    }

    public void refreshDevice() {

        mFileArray.clear();
        switch (CURRENT_PATH) {
            case "otg://":
                for (BaseFile file : RootHelp.getDocumentFilesList(CURRENT_PATH, getMainActivity())) {
                    mFileArray.add(file);
                }
                break;
            default:
                for (BaseFile file : RootHelp.getFilesList(CURRENT_PATH)) {
                    mFileArray.add(file);
                }
                break;
        }
        //
        switch ( mIsList){
            case 0 :
                mLayoutManager = new LinearLayoutManager(this);
                break;
            case 1 :
                mLayoutManager = new GridLayoutManager(this,1);
                break;
            case 2 :
                mLayoutManager = new GridLayoutManager(this,3);
                break;
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        //
        int sortAsc = 1;
        Collections.sort(mFileArray, new FileSorter(0, mSortType, sortAsc));
        mAdapter = new RecyclerAdapter(this,this, mFileArray , mIsList);
        //
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        //
        if (mFileArray.isEmpty()){
            mRecyclerView.setVisibility(View.INVISIBLE);
            mEmptyView.setVisibility(View.VISIBLE);
        }else{
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.INVISIBLE);
        }
        //
        try{
            mLayoutManager.scrollToPosition(mScrollPosition.get(mScrollPosition.size()-1));
        }catch (Exception ignore){
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mCopyFile == null) {
            getMenuInflater().inflate(R.menu.main, menu);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle(R.string.app_name);
        }else {
            getMenuInflater().inflate(R.menu.copy_menu, menu);
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle(R.string.paste_to);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_name:
                mSortType = 0;
                break;
            case R.id.sort_date:
                mSortType = 1;
                break;
            case R.id.sort_size:
                mSortType = 2;
                break;
            case R.id.sort_type:
                mSortType = 3;
                break;
            case R.id.grid:
                switch (mIsList){
                    case 0:
                        item.setIcon(getResources().getDrawable(R.drawable.ic_grid_off));
                        mIsList = 1;
                        break;
                    case 1:
                        item.setIcon(getResources().getDrawable(R.drawable.ic_action_list_2));
                        mIsList = 2;
                        break;
                    case 2:
                        item.setIcon(getResources().getDrawable(R.drawable.ic_grid_off));
                        mIsList = 0;
                        break;
                }
                break;
            case R.id.copy_confirm:
                break;
            case R.id.copy_cancel:
                mCopyFile = null;
                invalidateOptionsMenu();
                break;
        }
        refreshDevice();
        return true;
    }

    public void stopAnimation() {
        if ((!mAdapter.stoppedAnimation)) {
            for (int j = 0; j < mRecyclerView.getChildCount(); j++) {
                View v = mRecyclerView.getChildAt(j);
                if (v != null) v.clearAnimation();
            }
        }
        mAdapter.stoppedAnimation = true;
    }


    private class NavItemOnClickListener implements android.support.design.widget.NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Handle navigation view item clicks here.
            CURRENT_PATH = item.getTitle().toString();
            switch (item.getItemId()) {
                case R.id.select_all:
                    break;
                default:
                    break;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
                    runSAF();
            }
            mDrawerLayout.closeDrawer(GravityCompat.START);
            refreshDevice();
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (mActionMode!= null ) {
                mActionMode.finish();
            } else if (CURRENT_PATH.compareTo(getExternalStorageDirectory().toString())==0){
                exit();
                return;
            }
            CURRENT_PATH = getLastPath(CURRENT_PATH);
            refreshDevice();
            mScrollPosition.remove(mScrollPosition.size() - 1);
        }
    }

    public String getLastPath(String path) {
        File file = new File(path);
        return file.getParent();
    }

    public void exit() {
        if (backPressedToExitOnce) {
            finish();
        } else {
            this.backPressedToExitOnce = true;
            Toast.makeText(this, R.string.exit_toast, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressedToExitOnce = false;
                }
            }, 3000);
        }
    }

    public synchronized ArrayList<String> getStorageDirectories() {
        // Final set of paths
        final ArrayList<String> rv = new ArrayList<>();
        // Primary physical SD-CARD (not emulated)
        final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
        // All Secondary SD-CARDs (all exclude primary) separated by ":"
        final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
        // Primary emulated SD-CARD
        final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
        if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
            // Device has physical external storage; use plain paths.
            if (TextUtils.isEmpty(rawExternalStorage)) {
                // EXTERNAL_STORAGE undefined; falling back to default.
                rv.add("/storage/sdcard0");
            } else {
                rv.add(rawExternalStorage);
            }
        } else {
            // Device has emulated storage; external storage paths should have
            // userId burned into them.
            final String rawUserId;
            final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
            final String[] folders = Pattern.compile("/").split(path);
            final String lastFolder = folders[folders.length - 1];
            boolean isDigit = false;
            try {
                Integer.valueOf(lastFolder);
                isDigit = true;
            } catch (NumberFormatException ignored) {
            }
            rawUserId = isDigit ? lastFolder : "";
            // /storage/emulated/0[1,2,...]
            if (TextUtils.isEmpty(rawUserId)) {
                rv.add(rawEmulatedStorageTarget);
            } else {
                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
            }
        }
        // Add all secondary storages
        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
            // All Secondary SD-CARDs splited into array
            final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
            Collections.addAll(rv, rawSecondaryStorages);
        }
        if (SDK_INT >= Build.VERSION_CODES.M && checkStoragePermission())
            rv.clear();
        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String strings[] = getExtSdCardPathsForActivity(this);
            for (String s : strings) {
                File f = new File(s);
                if (!rv.contains(s) && canListFiles(f))
                    rv.add(s);
            }
        }
        File usb = getUsbDrive();
        if (usb != null && !rv.contains(usb.getPath())) rv.add(usb.getPath());

        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isUsbDeviceConnected()) rv.add("otg://");
        }
        return rv;
    }

    /**
     * Method finds whether a USB device is connected or not
     * @return true if device is connected
     */
    private boolean isUsbDeviceConnected() {
        UsbManager usbManager = (UsbManager) getSystemService(USB_SERVICE);

        if (usbManager.getDeviceList().size()!=0) {
            // we need to set this every time as there is no way to know that whether USB device was
            // disconnected after closing the app and another one was connected
            // in that case the uri will obviously change
            // other wise we could persist the uri even after reopening the app by not writing
            // this preference when it's not null
            sharedPref.edit().putString(KEY_PREF_OTG, "n/a").apply();
            return true;
        } else {
            sharedPref.edit().putString(KEY_PREF_OTG, null).apply();
            return false;
        }
    }

    public boolean checkStoragePermission() {
        // Verify that all required contact permissions have been granted.
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static String[] getExtSdCardPathsForActivity(Context context) {
        List<String> paths = new ArrayList<>();
        for (File file : context.getExternalFilesDirs("external")) {
            if (file != null) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                String path = file.getAbsolutePath().substring(0, index);
                try {
                    path = new File(path).getCanonicalPath();
                } catch (IOException e) {
                    // Keep non-canonical path.
                }
                paths.add(path);
            }
        }
        if (paths.isEmpty()) paths.add("/storage/sdcard1");
        return paths.toArray(new String[0]);
    }

    public static boolean canListFiles(File f) {
        try {
            return f.canRead() && f.isDirectory();
        } catch (Exception e) {
            return false;
        }
    }

    @SuppressLint("SdCardPath")
    public File getUsbDrive() {
        File parent = new File("/storage");

        try {
            for (File f : parent.listFiles())
                if (f.exists() && f.getName().toLowerCase().contains("usb") && f.canExecute())
                    return f;
        } catch (Exception ignored) {}

        parent = new File("/mnt/sdcard/usbStorage");
        if (parent.exists() && parent.canExecute())
            return (parent);
        parent = new File("/mnt/sdcard/usb_storage");
        if (parent.exists() && parent.canExecute())
            return parent;

        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void runSAF(){
        Intent safIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(safIntent, REQUEST_CODE_SAF);
    }

}
